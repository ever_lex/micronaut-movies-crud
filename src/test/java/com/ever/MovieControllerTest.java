package com.ever;

import io.micronaut.context.ApplicationContext;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.runtime.server.EmbeddedServer;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

public class MovieControllerTest {

    private static EmbeddedServer server;
    private static HttpClient client;

    @BeforeClass
    public static void setupServer(){
        server = ApplicationContext.build().run(EmbeddedServer.class);
        client = server.getApplicationContext().createBean(HttpClient.class, server.getURL());
    }

    @AfterClass
    public static void stopServer(){
        if (server != null){
            server.stop();
        }
        if (client != null){
            client.stop();
        }
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Test
    public void supplyAnInvalidOrderTriggersValidationFailure() {
        thrown.expect(HttpClientResponseException.class);
        thrown.expect(hasProperty("response", hasProperty("status", is(HttpStatus.BAD_REQUEST))));
        client.toBlocking().exchange(HttpRequest.GET("/movie/list"));
    }

    @Test
    public void testFindNonExistingGenreReturns404() {
        thrown.expect(HttpClientResponseException.class);
        thrown.expect(hasProperty("response", hasProperty("status", is(HttpStatus.NOT_FOUND))));
        HttpResponse response = client.toBlocking().exchange(HttpRequest.GET("/movie/99"));
    }

    @Test
    public void testAllOperations() {

        List<Integer> ids = new ArrayList<>();

        Movie movie = Movie.create(
                movieBuilder -> movieBuilder.
                        withtTitle("Matrix").
                        withYear(1999).
                        withGenre("Sci-Fi"));

        // create
        HttpRequest request = HttpRequest.POST("/movie", movie);
        HttpResponse response = client.toBlocking().exchange(request, Movie.class);

        ids.add( ((Movie) Objects.requireNonNull(response.body())).getId());
        Integer id = ids.get(0);

        assertEquals(HttpStatus.CREATED, response.getStatus());

        assertEquals(ids.size(), 1);

        // select
        request = HttpRequest.GET("/movie/"+id);
        movie = client.toBlocking().retrieve(request, Movie.class);

        assertEquals("Matrix", movie.getTitle());


      // update
        movie.setTitle("The Matrix");
        movie.setId(id);

        request = HttpRequest.PUT("/movie", movie);
        response = client.toBlocking().exchange(request);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatus());

        // list
        request = HttpRequest.GET("/movie");
        List<Movie> movies = client.toBlocking().retrieve(request, Argument.of(List.class, Movie.class));

        assertEquals(1, movies.size());

        // delete
        for (Integer movieId : ids) {
            request = HttpRequest.DELETE("/movie/" + movieId);
            response = client.toBlocking().exchange(request);
            assertEquals(HttpStatus.NO_CONTENT, response.getStatus());
        }

        request = HttpRequest.GET("/movie");
        movies = client.toBlocking().retrieve(request, Argument.of(List.class, Movie.class));
        assertEquals(0, movies.size());
    }

}
