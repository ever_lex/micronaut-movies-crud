package com.ever;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

@Controller("/")
public class HealthCheckController {

    @Get
    @Produces(MediaType.TEXT_PLAIN)
    public String home(){
        return "Micronault UP! ";
    }

}
