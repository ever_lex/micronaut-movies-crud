package com.ever;

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.runtime.ApplicationConfiguration;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;


@Singleton
public class MovieRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public MovieRepository(@CurrentSession EntityManager entityManager){
        this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    public Optional<Movie> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Movie.class, id));
    }

    @Transactional
    public Movie save(Movie Movie) {
        entityManager.persist(Movie);
        return Movie;
    }

    @Transactional
    public void deleteById(Integer id) {
        findById(id).ifPresent(Movie -> entityManager.remove(Movie));
    }

    @Transactional(readOnly = true)
    public List<Movie> findAll() {
        TypedQuery<Movie> query = entityManager.createQuery("SELECT m FROM Movie as m", Movie.class);
        query.setMaxResults(50);
        return query.getResultList();
    }

    @Transactional
    public int update(Integer id, String title, Integer year, String genre) {
        return entityManager.createQuery("UPDATE Movie e SET title = :title, year = :year, genre = :genre  where id = :id")
                .setParameter("title", title)
                .setParameter("year", year)
                .setParameter("genre", genre)
                .setParameter("id", id)
                .executeUpdate();
    }
}