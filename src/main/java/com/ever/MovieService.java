package com.ever;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;


@Singleton
public class MovieService {

    @Inject
    protected MovieRepository movieRepository;

    public Optional<Movie> findById(@NotNull Integer id) {
        return movieRepository.findById(id);
    }

    public Movie save(@NotNull Movie movie) {
        return movieRepository.save(movie);
    }

    public void deleteById(@NotNull Integer id) {
        movieRepository.deleteById(id);
    }

    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    public int update(Movie movie) {
        return movieRepository.update(movie.getId(), movie.getTitle(), movie.getYear(), movie.getGenre());
    }
}