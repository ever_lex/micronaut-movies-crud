package com.ever;

import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Controller("/movie")
public class MovieController {

    @Inject
    protected MovieService movieService;

    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{id}")
    public Movie show(Integer id) {
        return movieService.findById(id).orElse(null);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Get
    public List<Movie> list() {
        return movieService.findAll();
    }

    @Put
    public HttpResponse update(@Body @Valid Movie movie) {
        int nbrOfUpdated = movieService.update(movie);
        return HttpResponse
                .noContent()
                .header(HttpHeaders.LOCATION, toUri(movie).getPath());
    }

    @Post
    public HttpResponse<Movie> save(@Body @Valid Movie movie){
        movieService.save(movie);
        return HttpResponse
                .created(movie)
                .headers(headers -> headers.location(toUri(movie)));
    }

    @Delete("/{id}")
    public HttpResponse delete( Integer id) {
        movieService.deleteById(id);
        return HttpResponse.noContent();
    }

    private URI toUri(Movie Movie) {
        return URI.create("/movie/" + Movie.getId());
    }
}